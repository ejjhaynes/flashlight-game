[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/rhle/flashlight-game)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This is a quarantine project so we can play one of our favorite games over the internet. We’re using React with semantic-ui for all of the frontend and Firebase to handle hosting and the backend. 

## Getting Started

To get started, you can click the ready to code badge or clone the repo and run `yarn install`. Then run `yarn firebase login –no-localhost` to authenticate with firebase. I can add you to the existing project, or you can create your own Firebase project with Authentication, Realtime Database, and Hosting.

## Available Scripts

In the project directory, you can run:

### `yarn dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn lint`

Runs the linter. Automatically fixes some problems

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

