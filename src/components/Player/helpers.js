import { onceValue, setValue } from '../Firebase/database'

export const GAME_STATE = {
  LOBBY: 'lobby',
  DECLARE: 'declare',
  PLAYING: 'playing',
  PENDING: 'pending',
  END: 'end'
}

export function joinRoom (roomCode, user, onSuccess = () => {}, onFailure = () => {}) {
  if (roomCode) {
    onceValue('rooms/' + roomCode, (snapshot) => {
      const roomData = snapshot.val()
      if (roomData) {
        setValue('rooms/' + roomCode + '/users/' + user, true)
        setValue('users/' + user + '/activeGame', roomCode)
        onSuccess(true)
      } else {
        onFailure('Invalid Room Code')
      }
    })
  } else {
    onFailure('No Room Code')
  }
}
