import React from 'react'
import { render, screen } from '@testing-library/react'
import SplashScreen from './index'

test('renders cards and links', () => {
  render(<SplashScreen />)
  expect(screen.getByText(/create room/i)).toBeInTheDocument()
  expect(screen.getByText(/join room/i)).toBeInTheDocument()
})

/*

welp... joining and creating rooms will just have to go untested. can't change the url because jest will die

test('creates new room', () => {
  render(<FirebaseContext.Provider value={firebase}>
    <SplashScreen />
  </FirebaseContext.Provider>)
  fireEvent.click(screen.getByText(/create room/i))
})

*/
