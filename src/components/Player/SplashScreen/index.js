import React, { Component } from 'react'
import { Container, Card, Image, Input } from 'semantic-ui-react'

import { setValue } from '../../Firebase/database'
import { onAuth } from '../../Firebase/auth'
import { joinRoom, GAME_STATE } from '../helpers'

class SplashScreenBase extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: null,
      roomCodeField: '',
      joinLoading: false,
      joinError: ''
    }
  }

  static generateRoomCode () {
    const CHARS = 'bcdfghjklmnpqrstvxz1234567890'
    let ret = ''
    for (let i = 0; i < 6; i++) {
      ret = ret + CHARS.charAt(Math.floor(Math.random() * CHARS.length))
    }

    return ret
  }

  newRoomWrite () {
    const roomCode = SplashScreenBase.generateRoomCode()
    setValue('rooms/' + roomCode, {
      owner: this.state.user,
      creationTime: new Date().getTime(),
      users: {
        [this.state.user]: true
      },
      status: GAME_STATE.LOBBY
    })
    setValue('users/' + this.state.user + '/activeGame', roomCode)

    window.location.pathname = '/room/' + roomCode
  }

  joinRoomOnClick () {
    console.log('clicked join ', this.state.roomCodeField, this.state.user)
    this.setState({ joinLoading: true })
    joinRoom(
      this.state.roomCodeField,
      this.state.user,
      () => {
        window.location.pathname = '/room/' + this.state.roomCodeField
        this.setState({ joinLoading: false })
      },
      (failure) => this.setState({ joinError: failure, joinLoading: false })
    )
  }

  componentDidMount () {
    onAuth((user) => {
      if (user) {
        this.setState({ user: user.uid })
      } else {
        // No user is signed in.
      }
    })
  }

  render () {
    return (
      <Container>
        <Card.Group centered>

          <Card onClick={this.newRoomWrite.bind(this)}>
            <Image

              src="https://generative-placeholders.glitch.me/image?width=600&height=300&style=triangles&gap=30"
              wrapped ui={false}
            />
            <Card.Content>
              <Card.Header>Create Room</Card.Header>
              <Card.Description>
                                    Create a new room
              </Card.Description>
            </Card.Content>
          </Card>

          {/* <Link to='/join-game'> */}
          <Card>
            <Image src="https://generative-placeholders.glitch.me/image?width=600&height=300&style=123" wrapped ui={false} />
            <Card.Content>
              <Card.Header>Join Room</Card.Header>
              <Card.Description>
                <Input
                  action={{
                    content: 'Join',
                    onClick: this.joinRoomOnClick.bind(this)
                  }}
                  error={Boolean(this.state.joinError)}
                  placeholder='Room Code'
                  value={this.state.roomCodeField}
                  onChange={(e, data) => this.setState({ roomCodeField: data.value })}
                  loading={this.state.joinLoading}
                />
              </Card.Description>

            </Card.Content>
          </Card>
          {/* </Link> */}
        </Card.Group>
      </Container>
    )
  }
}

const SplashScreen = SplashScreenBase

export default SplashScreen
