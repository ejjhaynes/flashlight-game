import React, { Component } from 'react'
import { Container, Header, Icon, Form, Grid, Button } from 'semantic-ui-react'
import { onValueChange } from '../../Firebase/database'
import { CardDisplay } from './CardDisplay'
import { GAME_STATE } from './helpers'
import CardTypeCountDisplay from './CardTypeCountDisplay'
import PropTypes from 'prop-types'
class PlayerHandDisplayBase extends Component {
  constructor (props) {
    super(props)
    const { userId } = props
    this.state = {
      displayName: userId,
      editedCthulhus: null,
      editedSigns: null
    }
  }

  componentDidMount () {
    onValueChange('users/' + this.props.playerId, (snapshot) => {
      const v = snapshot.val()
      if (v) {
        this.setState({ displayName: v.name })
      }
    })
    onValueChange('rooms/' + this.props.roomCode + '/players/' + this.props.playerId + '/hand', (_) => {
      this.setState({
        editedCthulhus: null,
        editedSigns: null
      })
    })
  }

  render () {
    if ([this.props.playerId, this.props.playerData, this.props.userId].some((item) => item === null || item === undefined)) {
      return (<Container></Container>)
    }

    if ([this.props.playerData.hand].some((item) => item === null || item === undefined)) {
      return (<Container></Container>)
    }

    const disableEditDeclare = !this.props.canDeclare

    return (<Grid.Column>
      <Container textAlign='center'>

        {this.handHeader()}
        <Container>
          <Form>
            {this.declareInputFields(disableEditDeclare)}
            <Form.Group widths='equal'>
              {this.declareButton(disableEditDeclare)}
            </Form.Group>
          </Form>
        </Container>

        <Container>
          <Button.Group centered textAlign='center' fluid size='mini' compact>
            {this.turnHandIntoCardDisplays()}
          </Button.Group>
        </Container>

      </Container>
    </Grid.Column>)
  }

  turnHandIntoCardDisplays () {
    return this.props.playerData.hand
      .map((cardNumber) => (
        <CardDisplay
          cardNumber={cardNumber}
          key={cardNumber}
          cardOnMouseEnter={this.props.cardOnMouseEnter}
          cardOnMouseLeave={this.props.cardOnMouseLeave}
          cardColor={this.props.cardNumberToColor(cardNumber)}
          cardOnClick={this.props.cardOnClick}
          cardDisplayValue={(
            this.props.discoveredCards &&
            this.props.discoveredCards.indexOf(cardNumber) >= 0
              ? this.props.cardNumberToValue(cardNumber)
              : '?')
          } />))
  }

  handHeader () {
    const sortedCards = this.props.playerData.hand.slice()
    sortedCards.sort((a, b) => Number(a) - Number(b))
    return (<Header as='h2' icon textAlign='center'>
      <Icon name={this.props.playerIcon} color={this.props.playerColor} circular />
      <Header.Content>{this.state.displayName} {(this.props.playerId === this.props.userId ? ' (You)' : '')}</Header.Content>
      <PlayerInformation
        sortedCards={sortedCards}
        playerId={this.props.playerId}
        userId={this.props.userId}
        playerRole={this.props.playerData.role}
        cardNumberToValue={this.props.cardNumberToValue}
        gameState={this.props.gameState}
      />
    </Header>)
  }

  declareButton (disableEditDeclare) {
    return (this.props.playerId === this.props.userId
      ? (<Form.Button disabled={disableEditDeclare} onClick={() => { this.props.onDeclare(this.props.playerId, this.state.editedCthulhus, this.state.editedSigns) } }>
                        Declare
      </Form.Button>)
      : (<div></div>))
  }

  declareInputFields (disableEditDeclare) {
    if (
      this.props.playerId === this.props.userId
    ) {
      return (
        <Form.Group inline widths='equal'>
          <Form.Input
            fluid
            type={'number'}
            placeholder='Cthulhus'
            label='Cthulhus'
            size='mini'
            disabled={disableEditDeclare}
            value={(Number.isInteger(this.state.editedCthulhus) ? this.state.editedCthulhus : this.props.playerData.claims.cthulhus)}
            onChange={(_, data) => this.setState({ editedCthulhus: (Number(data.value) > -1 && Number(data.value) < 6 ? Number(data.value) : 0) })} />
          <Form.Input
            fluid
            type={'number'}
            placeholder='Signs'
            label='Signs'
            size='mini'
            disabled={disableEditDeclare}
            value={(Number.isInteger(this.state.editedSigns) ? this.state.editedSigns : this.props.playerData.claims.signs)}
            onChange={(_, data) => this.setState({ editedSigns: (Number(data.value) > -1 && Number(data.value) < 6 ? Number(data.value) : 0) })} />
        </Form.Group>
      )
    } else {
      return (
        <CardTypeCountDisplay cthulhus={this.props.playerData.claims.cthulhus} signs={this.props.playerData.claims.signs} />
      )
    }
  }
}

PlayerHandDisplayBase.propTypes = {
  userId: PropTypes.string,
  playerId: PropTypes.string,
  playerData: PropTypes.shape(
    {
      hand: PropTypes.arrayOf(PropTypes.number),
      role: PropTypes.string,
      claims: PropTypes.shape({
        signs: PropTypes.number,
        cthulhus: PropTypes.number
      })
    }
  ),
  cardOnMouseEnter: PropTypes.func,
  cardOnMouseLeave: PropTypes.func,
  cardOnClick: PropTypes.func,
  activeCard: PropTypes.number,
  discoveredCards: PropTypes.arrayOf(PropTypes.number),
  cardNumberToValue: PropTypes.func,
  numPlayers: PropTypes.number,
  canDeclare: PropTypes.bool,
  onDeclare: PropTypes.func,
  playerIcon: PropTypes.string,
  playerColor: PropTypes.string,
  cardNumberToColor: PropTypes.func,
  gameState: PropTypes.string,
  roomCode: PropTypes.string
}

export function PlayerInformation ({ sortedCards, playerId, userId, playerRole, cardNumberToValue, gameState }) {
  if (playerId === userId || gameState === GAME_STATE.END) {
    return (<Container>
      <Header sub>
        {playerRole}
      </Header>
      <Header as='h3' sub>
        {sortedCards.map((cardNumber) => cardNumberToValue(cardNumber)).join(' ')}
      </Header>
    </Container>)
  }
  return (<Container></Container>)
}

PlayerInformation.propTypes = {
  sortedCards: PropTypes.arrayOf(PropTypes.number),
  playerId: PropTypes.string,
  userId: PropTypes.string,
  playerRole: PropTypes.string,
  cardNumberToValue: PropTypes.func,
  gameState: PropTypes.string
}

const PlayerHandDisplay = PlayerHandDisplayBase
export default PlayerHandDisplay
