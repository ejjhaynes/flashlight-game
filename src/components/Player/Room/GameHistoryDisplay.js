import React, { Component } from 'react'
import { formatDistanceToNow } from 'date-fns'
import { Feed, Card, Pagination, Container } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { onValueChange } from '../../Firebase/database'

class GameHistoryDisplayBase extends Component {
  constructor (props) {
    super(props)

    this.state = {
      day: (this.props.roomData ? this.props.roomData.day : 1)
    }
  }

  componentDidMount () {
    onValueChange('rooms/' + this.props.roomCode + '/day', (snapshot) => {
      const v = snapshot.val()
      if (v) {
        this.setState({ day: v })
      }
    })
  }

  handlePaginationChange (_, { activePage }) {
    this.setState({ day: activePage })
  }

  render () {
    if (!this.props.roomData || !this.props.roomData.history) {
      return (<React.Fragment></React.Fragment>)
    }
    // console.log(Object.values(roomData.history))
    const history = Object.values(this.props.roomData.history)
    // if(
    //   Array.isArray(roomData.history)
    // ) {
    //   history = roomData.history
    // } else {
    //   history = []
    // }
    history.reverse()
    const events = history.filter(
      (i) => i.day === this.state.day
    ).map((i) => (<HistoryEvent history={i} players={this.props.players} cardNumberToType={this.props.cardNumberToType} key={i.timestamp}/>))
    return (
      <Card fluid>
        <Card.Content>
          <Card.Header>Recent Game History</Card.Header>
        </Card.Content>
        <Card.Content>
          <Feed>
            {events}
          </Feed>
        </Card.Content>
        <Container fluid textAlign='center'>
          <Pagination
            activePage={this.state.day}
            onPageChange={this.handlePaginationChange.bind(this)}
            firstItem={null}
            lastItem={null}
            totalPages={this.props.roomData.day}
          />
        </Container>
      </Card>

    )
  }
}
function HistoryEvent ({ history, players, cardNumberToType }) {
  const { action, timestamp } = history
  let t = timestamp
  if (!t) {
    t = new Date()
  } else {
    t = new Date(t)
  }
  return (<Feed.Event>
    <Feed.Label icon={historyActionToLabel(action)} />
    <Feed.Content>
      <Feed.Summary content={historyEventToString(history, players, cardNumberToType)} />
      <Feed.Date>{formatDistanceToNow(t, { includeSeconds: true, addSuffix: true })}</Feed.Date>
    </Feed.Content>
  </Feed.Event>)
}
function historyActionToLabel (action) {
  switch (action) {
    case 'BEGIN DAY':
      return 'sun'
    case 'DECLARE':
      return 'talk'
    case 'DISCOVER':
      return 'find'
    case 'GAME OVER':
      return 'trophy'
    default:
      return 'question'
  }
}
function historyEventToString (history, players, cardNumberToType) {
  const { action, data, user } = history
  let displayActor = 'UNNAMED'
  const c = data.cthulhus
  const s = data.signs
  let claimString = ''
  let displayTarget = 'UNNAMED'
  if (players && players[user]) {
    displayActor = players[user]
  }
  switch (action) {
    case 'BEGIN DAY':
      return 'Start of Day ' + data.day
    case 'DECLARE':
      if (c === 0 && s === 0) {
        claimString = 'nothing'
      } else {
        if (c > 0) {
          claimString += ' ' + c.toString() + ' cthulhu' + (c === 1 ? '' : 's')
        }
        if (c > 0 && s > 0) {
          claimString += ' and'
        }
        if (s > 0) {
          claimString += ' ' + s.toString() + ' sign' + (s === 1 ? '' : 's')
        }
      }
      return displayActor + ' claims ' + claimString
    case 'DISCOVER':
      if (players && players[data.targetPlayer]) {
        displayTarget = players[data.targetPlayer]
      }
      return displayActor + ' discovers ' + displayTarget + '\'s ' + cardNumberToType(data.card)
    case 'GAME OVER':
      return data.winner + ' win!'
    default:
      return displayActor + ' does ' + action + ' ' + JSON.stringify(data)
  }
}

GameHistoryDisplayBase.propTypes = {
  roomData: PropTypes.shape({
    day: PropTypes.number,
    history: PropTypes.arrayOf(
      PropTypes.shape({
        action: PropTypes.string,
        timestamp: PropTypes.object
      })
    )
  }),
  players: PropTypes.object,
  cardNumberToType: PropTypes.func,
  roomCode: PropTypes.string
}

HistoryEvent.propTypes = {
  history: PropTypes.shape({
    action: PropTypes.string,
    timestamp: PropTypes.object
  }),
  players: PropTypes.object,
  cardNumberToType: PropTypes.func
}

export default GameHistoryDisplayBase
