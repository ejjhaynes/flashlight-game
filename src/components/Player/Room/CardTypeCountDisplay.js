import React from 'react'
import { Container, Label } from 'semantic-ui-react'
import PropTypes from 'prop-types'

export default function CardTypeCountDisplay ({ cthulhus, signs }) {
  return (
    <Container textAlign='center'>
      <Label color='red'>
            Cthulhus
        <Label.Detail>{cthulhus}</Label.Detail>
      </Label>
      <Label color='green'>
            Signs
        <Label.Detail>{signs}</Label.Detail>
      </Label>
    </Container>
  )
}

CardTypeCountDisplay.propTypes = {
  cthulhus: PropTypes.number,
  signs: PropTypes.number
}
