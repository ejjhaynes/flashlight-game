import React, { Component } from 'react'
import { Card, Input, Header, Icon, Segment } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { onValueChange, setValue } from '../../Firebase/database'

export default function WaitingRoomPlayersDisplay (props) {
  var users = []
  if (props.roomData !== null) {
    users = Object.keys(props.roomData.users)
      .map((i) => (<WaitingPlayerDisplay userId={i} myUserId={props.userId} key={i} />))
  }
  return (<Segment textAlign='center'>
    <Header as='h2' icon textAlign='center'>
      <Icon name='users' circular />
      <Header.Content>Players</Header.Content>
    </Header>
    <Card.Group centered>
      {users}
    </Card.Group>

  </Segment>)
}

WaitingRoomPlayersDisplay.propTypes = {
  roomData: PropTypes.object,
  userId: PropTypes.string
}

export class WaitingPlayerDisplay extends Component {
  constructor (props) {
    super(props)
    this.state = {
      displayName: '',
      displayNameField: ''
    }
  }

  componentDidMount () {
    onValueChange('users/' + this.props.userId + '/name', (snapshot) => {
      if (snapshot.val()) {
        this.setState({ displayName: snapshot.val(), displayNameField: snapshot.val() })
      }
    })
  }

  displayNameFieldOnChange (_, data) {
    this.setState({ displayNameField: data.value })
  }

  submitNewName () {
    setValue('/users/' + this.props.userId + '/name', String(this.state.displayNameField).toString().slice(0, 24))
  }

  render () {
    const isNameChange = this.state.displayNameField !== this.state.displayName
    if (this.props.userId === this.props.myUserId) {
      return (<Card key={this.props.userId}>
        <Card.Content>
          <Card.Header>You!</Card.Header>
          <Input
            action={{
              icon: (isNameChange ? 'check circle outline' : 'check circle'),
              color: (isNameChange ? 'teal' : null),
              disabled: !isNameChange,
              onClick: this.submitNewName.bind(this)
            }}
            placeholder='Your name'
            value={this.state.displayNameField}
            onChange={this.displayNameFieldOnChange.bind(this)}
          />
          <p>Probably a cultist</p>
        </Card.Content>
      </Card>)
    }
    return (<Card key={this.props.userId}>
      <Card.Content>
        <Card.Header>{this.state.displayName}</Card.Header>
                    Probably a cultist
      </Card.Content>
    </Card>)
  }
}

WaitingPlayerDisplay.propTypes = {
  userId: PropTypes.string,
  myUserId: PropTypes.string
}
