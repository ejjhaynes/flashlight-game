import React from 'react'
import { render, screen } from '@testing-library/react'
import { WaitingPlayerDisplay } from './WaitingRoomPlayersDisplay'
import userEvent from '@testing-library/user-event'
import { onValueChange, setValue } from '../../Firebase/database'
jest.mock('../../Firebase/database')

describe('WaitingPlayerDisplay', () => {
  beforeEach(() => {
    onValueChange.mockClear()
    setValue.mockClear()
  })

  test('renders own display', () => {
    render(<WaitingPlayerDisplay
      userId='a'
      myUserId='a'
    />)
    expect(screen.getByText('You!')).toBeInTheDocument()
    expect(setValue.mock.calls.length).toBe(0)
  })

  test('no submit on name change', () => {
    render(<WaitingPlayerDisplay
      userId='a'
      myUserId='a'
    />)
    userEvent.type(screen.getByRole('textbox'), 'name change')
    expect(setValue.mock.calls.length).toBe(0)
  })

  test('disabled button without change', () => {
    render(<WaitingPlayerDisplay
      userId='a'
      myUserId='a'
    />)
    userEvent.click(screen.getByRole('button'))
    expect(setValue.mock.calls.length).toBe(0)
  })

  test('button works with name change', () => {
    render(<WaitingPlayerDisplay
      userId='a'
      myUserId='a'
    />)
    userEvent.type(screen.getByRole('textbox'), 'name change')
    userEvent.click(screen.getByRole('button'))
    expect(setValue.mock.calls.length).toBe(1)
  })

  test('renders other', () => {
    render(<WaitingPlayerDisplay
      userId='b'
      myUserId='a'
    />)
    expect(screen.queryByText('You!')).not.toBeInTheDocument()
    expect(screen.queryByRole('button')).not.toBeInTheDocument()
    expect(screen.queryByRole('textbox')).not.toBeInTheDocument()
    expect(setValue.mock.calls.length).toBe(0)
  })
})
