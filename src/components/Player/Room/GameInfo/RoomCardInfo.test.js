import React from 'react'
import { render, screen } from '@testing-library/react'
import { GAME_STATE } from '../helpers'
import RoomCardInfo from './RoomCardInfo'
import renderer from 'react-test-renderer'

describe('Card Count Display', () => {
  test('renders', () => {
    render(<RoomCardInfo
      numPlayers={4}
      discoveredCards={[1, 2]}
      claims={[]}
      gameState={GAME_STATE.PLAYING}
    />)
    expect(screen.getByText('Game Info')).toBeInTheDocument()
    expect(screen.getByText('4')).toBeInTheDocument()
    expect(screen.getByText('2')).toBeInTheDocument()
  })

  test('nothing for missing/lobby state', () => {
    const tree = renderer.create(<RoomCardInfo/>).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
