import React from 'react'
import { Card, Header, Container, Label } from 'semantic-ui-react'
import PropTypes from 'prop-types'

export default function DiscrepancyInfo ({ numPlayers, numCuthulusClaimed, numSignsClaimed, numSignsDiscovered }) {
  return (<Card fluid>
    <Card.Content>
      <Header as='h4'>Discrepancy</Header>
      <Container textAlign='center'>
        {renderComparisonMessage(numCuthulusClaimed, 1, 'Cthulu', 'red')}
        {renderComparisonMessage(numSignsClaimed, numPlayers - numSignsDiscovered, 'sign', 'green')}
      </Container>
    </Card.Content>
  </Card>)
}

function renderComparisonMessage (numClaimed, numExpected, type, labelColor) {
  if (numClaimed > numExpected) {
    return <Label color={labelColor}>{numExpected - numClaimed + 'too few ' + type + 's!'}</Label>
  } else if (numClaimed < numExpected) {
    return <Label color={labelColor}>{numClaimed - numExpected + 'too many ' + type + 's!'}</Label>
  } else {
    return <React.Fragment />
  }
}

DiscrepancyInfo.propTypes = {
  numPlayers: PropTypes.number,
  numCuthulusClaimed: PropTypes.number,
  numSignsClaimed: PropTypes.number,
  numSignsDiscovered: PropTypes.number
}
