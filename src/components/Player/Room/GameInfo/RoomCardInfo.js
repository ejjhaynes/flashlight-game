import React from 'react'
import { Card, Header } from 'semantic-ui-react'
import { typesDiscovered, GAME_STATE } from '../helpers'
import CardTypeCountDisplay from '../CardTypeCountDisplay'
import PropTypes from 'prop-types'
import DiscrepancyInfo from './DiscrepancyInfo'

export default function RoomCardInfo ({ numPlayers, discoveredCards = [], claims = [], gameState = GAME_STATE.LOBBY }) {
  const { cthulhus, signs } = typesDiscovered(discoveredCards, numPlayers)

  if (gameState !== GAME_STATE.LOBBY) {
    const numCuthulusClaimed = claims.map((i) => i.cthulhus).reduce((a, c) => a + c, 0)
    const numSignsClaimed = claims.map((i) => i.signs).reduce((a, c) => a + c, 0)
    const shouldRenderDiscrepancy = gameState === GAME_STATE.PLAYING && (numCuthulusClaimed > 0 || numSignsClaimed > 0)

    return (
      <Card fluid>
        <Card.Content>
          <Card.Header>Game Info</Card.Header>
        </Card.Content>
        <Card.Content>
          <Header as='h4'>Cards in Game</Header>
          <CardTypeCountDisplay cthulhus={1} signs={numPlayers} />
        </Card.Content>
        <Card.Content>
          <Header as='h4'>Discovered Cards</Header>
          <CardTypeCountDisplay cthulhus={cthulhus} signs={signs} />
        </Card.Content>
        <Card.Content>
          <Header as='h4'>Claimed Cards</Header>
          <CardTypeCountDisplay cthulhus={numCuthulusClaimed} signs={numSignsClaimed} />
        </Card.Content>
        { shouldRenderDiscrepancy ? <DiscrepancyInfo numPlayers={numPlayers} numCuthulusClaimed={numCuthulusClaimed} numSignsClaimed={numSignsClaimed} numSignsDiscovered={signs} /> : <React.Fragment/>}
      </Card>
    )
  } else {
    return (
      <React.Fragment/>
    )
  }
}

RoomCardInfo.propTypes = {
  numPlayers: PropTypes.number,
  discoveredCards: PropTypes.array,
  claims: PropTypes.array,
  gameState: PropTypes.string
}
