import React, { Component } from 'react'
import { Container, Card, Header, Icon, Segment, Button, Form } from 'semantic-ui-react'
import { shuffle, CULTIST, INVESTIGATOR, startDay } from './helpers'
import { onValueChange, setValue } from '../../Firebase/database'
import { joinRoom } from '../helpers'
import PropTypes from 'prop-types'

function initGame (roomCode, roomData, numCultists, numInvestigators) {
  if (roomCode == null || roomCode === undefined || roomData === null || roomData === undefined) {
    return
  }
  const prefix = 'rooms/' + roomCode
  setValue(prefix + '/status', 'loading')
  let users = Object.keys(roomData.users)
  let rolePool = new Array(numCultists).fill(CULTIST).concat(new Array(numInvestigators).fill(INVESTIGATOR))
  rolePool = shuffle(rolePool)
  const players = {}
  users.forEach((userId) => {
    players[userId] = {
      role: rolePool.pop(),
      claims: {
        cthulhus: 0,
        signs: 0
      }
    }
  })

  const roomDataMap = new Map()
  users = shuffle(users)
  roomDataMap.set('players', players)
  roomDataMap.set('playerOrder', users)
  roomDataMap.set('currentPlayer', users[0])
  roomDataMap.set('day', 1)
  roomDataMap.set('day1', '')
  roomDataMap.set('day2', '')
  roomDataMap.set('day3', '')
  roomDataMap.set('day4', '')
  roomDataMap.set('history', [])
  setValue(prefix, roomDataMap, false)
  startDay(1, users, [], roomCode)
  setValue(prefix + '/status', 'declare')
}

function defaultCultistsForPlayerCount (playerCount) {
  if (playerCount > 6) {
    return 3
  }

  return 2
}

function defaultInvesigatorsForPlayerCount (playerCount) {
  if (playerCount > 7) {
    return 6
  }
  if (playerCount > 5) {
    return 5
  }
  if (playerCount === 5) {
    return 4
  }
  return 3
}

class RoomInfoDisplay extends Component {
  constructor (props) {
    super(props)

    this.state = {
      cultists: 0,
      investigators: 0,
      playerCount: (this.props.roomData !== null ? Object.keys(this.props.roomData.users).length : 0)
    }
  }

  componentDidMount () {
    onValueChange('rooms/' + this.props.roomCode + '/users', (snapshot) => {
      if (snapshot.val()) {
        const playerCount = Object.keys(snapshot.val()).length
        this.setState(
          {
            playerCount: playerCount,
            cultists: defaultCultistsForPlayerCount(playerCount),
            investigators: defaultInvesigatorsForPlayerCount(playerCount)
          }
        )
      }
    })
  }

  updateCultists (_, data) {
    this.setState({ cultists: Number(data.value) })
  }

  updateInvestigators (_, data) {
    this.setState({ investigators: Number(data.value) })
  }

  render () {
    if (this.props.roomData === null) {
      return (<Container></Container>)
    }
    const players = Object.keys(this.props.roomData.users)
    const isPlayerMember = players.includes(this.props.userId)
    const playerCount = (this.props.roomData !== null ? players.length : 0)
    const isRoomOwner = this.props.userId === this.props.roomData.owner
    const startDisabled = playerCount < 4 || playerCount > (this.state.investigators + this.state.cultists)
    const startButton = (isRoomOwner ? (
      <Button disabled={startDisabled} primary
        onClick={() => initGame(this.props.roomCode, this.props.roomData, this.state.cultists, this.state.investigators)}
        key='startButton'>
        Start Game
      </Button>) : null)
    const joinRoomButton = (isRoomOwner || isPlayerMember ? null : (<Button primary key='joinRoomButton' onClick={() => joinRoom(this.props.roomCode, this.props.userId)}>Join Room</Button>))
    if (startButton === null && joinRoomButton === null) {
      return <React.Fragment/>
    }
    return (<Segment textAlign='center'>
      <Header as='h2' icon textAlign='center'>
        <Icon name='settings' circular />
        <Header.Content>Settings</Header.Content>
      </Header>
      <Card.Group centered>
        <Card>
          <Card.Content>
            <Card.Header>Room Code</Card.Header>
            {this.props.roomCode}
          </Card.Content>
        </Card>
        <SetPlayerRolePool
          cultists={this.state.cultists}
          investigators={this.state.investigators}
          updateCultists={this.updateCultists.bind(this)}
          updateInvestigators={this.updateInvestigators.bind(this)}
          showWhen={isRoomOwner}
        />
        <Card>
          <Card.Content>
            {[startButton, joinRoomButton]}
          </Card.Content>
        </Card>
      </Card.Group>

    </Segment>)
  }
}

function SetPlayerRolePool ({ cultists, investigators, updateCultists, updateInvestigators, showWhen }) {
  if (showWhen) {
    return (
      <Card>
        <Card.Content>
          <Card.Header>New Game Settings</Card.Header>
          <Form.Group inline widths='equal'>
            <Form.Input
              fluid
              type={'number'}
              placeholder='Cultists'
              label='Cultists'
              value={cultists}
              onChange={updateCultists} />
            <Form.Input
              fluid
              type={'number'}
              placeholder='Investigators'
              label='Investigators'
              value={investigators}
              onChange={updateInvestigators} />
          </Form.Group>
        </Card.Content>
      </Card>

    )
  } else {
    return (
      <React.Fragment></React.Fragment>
    )
  }
}

RoomInfoDisplay.propTypes = {
  roomData: PropTypes.object,
  userId: PropTypes.string,
  roomCode: PropTypes.string
}

export default RoomInfoDisplay
