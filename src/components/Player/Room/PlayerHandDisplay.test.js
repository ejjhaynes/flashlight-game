import React from 'react'
import { render, screen } from '@testing-library/react'
import PlayerHandDisplay, { PlayerInformation } from './PlayerHandDisplay'
import { GAME_STATE } from './helpers'
import renderer from 'react-test-renderer'

const numToType = jest.fn((num) => (num === 0 ? 'A' : 'B'))

describe('PlayerInformation', () => {
  test('shows for user player', () => {
    render(<PlayerInformation
      sortedCards={[0, 1, 4, 5, 6]}
      playerId={'a'}
      userId={'a'}
      playerRole='role text'
      cardNumberToValue={numToType}
    />)
    expect(screen.getByText('role text')).toBeInTheDocument()
    expect(screen.getByText('B', { exact: false })).toBeInTheDocument()
    expect(screen.getByText('A', { exact: false })).toBeInTheDocument()
  })

  test('hides for non user player', () => {
    render(<PlayerInformation
      sortedCards={[0, 1, 4, 5, 6]}
      playerId={'a'}
      userId={'b'}
      playerRole='role text'
      cardNumberToValue={numToType}
    />)
    expect(screen.queryByText('role text')).not.toBeInTheDocument()
    expect(screen.queryByText('B')).not.toBeInTheDocument()
    expect(screen.queryByText('A')).not.toBeInTheDocument()
  })

  test('shows for non user player on gameover', () => {
    render(<PlayerInformation
      sortedCards={[0, 1, 4, 5, 6]}
      playerId={'a'}
      userId={'b'}
      playerRole='role text'
      cardNumberToValue={numToType}
      gameState={GAME_STATE.END}
    />)
    expect(screen.getByText('role text')).toBeInTheDocument()
    expect(screen.getByText('B', { exact: false })).toBeInTheDocument()
    expect(screen.getByText('A', { exact: false })).toBeInTheDocument()
  })
})

describe('PlayerHandDisplay', () => {
  test('handles missing props', () => {
    const tree = renderer.create(
      <PlayerHandDisplay />
    ).toJSON()

    expect(tree).toMatchSnapshot()
  })

  test('renders', () => {
    render(
      <PlayerHandDisplay
        userId='a'
        playerId='a'
        playerData={
          {
            hand: [1, 2, 3, 4],
            role: 'CULTIST',
            claims: {
              signs: 0,
              cthulhus: 0
            }
          }
        }
        cardNumberToColor={jest.fn()}
        cardNumberToValue={numToType}
      />
    )
  })
})
