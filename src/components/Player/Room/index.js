import React, { Component } from 'react'
import { Container, Grid, Rail } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { useParams } from 'react-router-dom'

import { onValueChange, setValue, pushValue, onceValue } from '../../Firebase/database'
import { createTrace } from '../../Firebase/perf'
import WaitingRoomPlayersDisplay from './WaitingRoomPlayersDisplay'
import RoomInfoDisplay from './RoomInfoDisplay'
import PlayerHandDisplay from './PlayerHandDisplay'
import { DiscoveredDaysDisplay } from './DiscoveredDaysDisplay'
import { cardNumberToType, checkWinner, SYSTEM_USER, GAME_STATE, addHistory, startDay } from './helpers'
import GameHistoryDisplay from './GameHistoryDisplay'
import RoomCardInfo from './GameInfo/RoomCardInfo'

class RoomBase extends Component {
  constructor (props) {
    super(props)

    this.state = {
      roomData: null,
      discoveredCards: [],
      players: {}
    }
  }

  componentDidMount () {
    const loadDataFunc = (this.props.isHistory ? onceValue : onValueChange)
    const locationPrefix = (this.props.isHistory ? 'history/' : 'rooms/')
    loadDataFunc(locationPrefix + this.props.roomCode, (snapshot) => {
      const roomData = snapshot.val()

      if (roomData) {
        this.setDiscoveredCards(roomData)
        this.setPlayerNames(roomData)
        this.setState({
          roomData: roomData
        })
      }
    })
  }

  setDiscoveredCards (roomData) {
    if (roomData.day1) {
      const day1 = Object.values(roomData.day1)
      const day2 = Object.values(roomData.day2)
      const day3 = Object.values(roomData.day3)
      const day4 = Object.values(roomData.day4)
      const allDiscoveredCards = [day1, day2, day3, day4]
        .flat()
        .filter(Number.isInteger)
      this.setState({
        discoveredCards: allDiscoveredCards
      })
    } else {
      this.setState({ discoveredCards: [] })
    }
  }

  setPlayerNames (roomData) {
    if (roomData.players) {
      Object.keys(roomData.players).forEach((i) => {
        onValueChange('users/' + i, (snapshot) => {
          const playerData = snapshot.val()
          if (playerData) {
            const players = this.state.players
            players[i] = playerData.name
            this.setState({
              players: players
            })
          }
        })
      })
    }
  }

  addHistory (actor, action, data) {
    return addHistory(this.props.roomCode, actor, action, data, this.state.roomData.day)
  }

  onDeclare (player, cthulhus, signs) {
    setValue('/rooms/' + this.props.roomCode + '/players/' + player + '/claims', {
      cthulhus: (Number.isInteger(cthulhus) ? cthulhus : 0),
      signs: (Number.isInteger(signs) ? signs : 0)
    })
    this.addHistory(
      player,
      'DECLARE',
      {
        cthulhus: (Number.isInteger(cthulhus) ? cthulhus : 0),
        signs: (Number.isInteger(signs) ? signs : 0)
      }
    )

    if (this.state.roomData.status === GAME_STATE.DECLARE) {
      this.changeDeclarePlayer(player)
    }
  }

  changeDeclarePlayer (player) {
    const numDeclares = (Number.isInteger(this.state.roomData.numDeclares) ? Number(this.state.roomData.numDeclares) : 0) + 1
    const playerIndex = this.state.roomData.playerOrder.indexOf(player)
    const nextPlayerIndex = (playerIndex + 1) % this.state.roomData.playerOrder.length
    const nextPlayer = this.state.roomData.playerOrder[nextPlayerIndex]
    console.log('before ', this.state.roomData.numDeclares)
    console.log('after ', numDeclares)
    setValue('/rooms/' + this.props.roomCode + '/numDeclares', numDeclares)
    if (numDeclares >= this.state.roomData.playerOrder.length) {
      setValue('/rooms/' + this.props.roomCode + '/status', GAME_STATE.PLAYING)
    }
    setValue('/rooms/' + this.props.roomCode + '/currentPlayer', nextPlayer)
  }

  cardOnMouseEnter (userId, cardNumber) {
    if (userId === this.state.roomData.currentPlayer) {
      setValue('/rooms/' + this.props.roomCode + '/activeCard', cardNumber)
    }
  }

  cardOnMouseLeave (userId) {
    if (userId === this.state.roomData.currentPlayer) {
      setValue('/rooms/' + this.props.roomCode + '/activeCard', '')
    }
  }

  cardOnClick (userId, cardNumber, cardOwnerId) {
    if (userId === this.state.roomData.currentPlayer && this.state.roomData.status === GAME_STATE.PLAYING) {
      if (
        !this.state.roomData.players[userId].hand.includes(cardNumber) &&
                !this.state.discoveredCards.includes(cardNumber)
      ) {
        this.playCard(userId, cardOwnerId, cardNumber)
      }
    }
  }

  playCard (userId, cardOwnerId, cardNumber) {
    this.addHistory(userId, 'DISCOVER', {
      targetPlayer: cardOwnerId,
      card: cardNumber
    })
    let discoveredToday = this.state.roomData['day' + this.state.roomData.day]
    if (!Array.isArray(discoveredToday)) {
      discoveredToday = []
    }
    discoveredToday.push(cardNumber)

    const winner = checkWinner(this.state.discoveredCards.concat(cardNumber), this.state.roomData.playerOrder.length)

    setValue('/rooms/' + this.props.roomCode + '/day' + this.state.roomData.day, discoveredToday)

    if (winner) {
      setValue('/rooms/' + this.props.roomCode + '/status', GAME_STATE.END)
      this.addHistory(SYSTEM_USER, 'GAME OVER', {
        winner: winner + 's'
      }).then(() => {
        const historyRef = pushValue('/history', this.state.roomData)
        // Welcome to callback hell
        this.state.roomData.playerOrder.forEach((userId) => {
          const playerArchiveLocation = '/users/' + userId + '/archive'
          onceValue(playerArchiveLocation).then((snapshot) => {
            const ret = [historyRef.key]
            if (snapshot.val()) {
              ret.push(snapshot.val())
            }
            setValue(playerArchiveLocation, ret.flat())
          })
        })
      })
    } else {
      setValue('/rooms/' + this.props.roomCode + '/currentPlayer', cardOwnerId)
      if (discoveredToday.length === this.state.roomData.playerOrder.length) {
        this.changeDay(cardNumber)
      }
    }
  }

  changeDay (cardNumber) {
    setValue('/rooms/' + this.props.roomCode + '/status', GAME_STATE.PENDING)
    setTimeout(() => {
      startDay(
        this.state.roomData.day + 1,
        this.state.roomData.playerOrder,
        this.state.discoveredCards.concat(cardNumber),
        this.props.roomCode
      )
    }, 5000)
  }

  cardNumberToColor (cardNumber) {
    if (this.state.roomData.activeCard === cardNumber) {
      return 'purple'
    }

    if (this.state.discoveredCards && this.state.discoveredCards.indexOf(cardNumber) >= 0) {
      const cardString = cardNumberToType(cardNumber, this.state.roomData.playerOrder.length)
      switch (cardString) {
        case 'C':
          return 'red'
        case 'S':
          return 'green'
        default:
          return 'grey'
      }
    }
  }

  render () {
    const trace = createTrace('renderGameRoom')
    trace.start()
    let playerHands, numPlayers, claims, gameState

    if (this.state.roomData && this.state.roomData.players && this.state.roomData.playerOrder) {
      claims = Object.values(this.state.roomData.players).map((i) => i.claims)
      numPlayers = this.state.roomData.playerOrder.length
      gameState = this.state.roomData.status
      playerHands = this.state.roomData.playerOrder
        .map(
          (key) => (
            <PlayerHandDisplay
              playerId={key}
              playerData={this.state.roomData.players[key]}
              userId={this.props.user}
              key={key}
              cardNumberToValue={(cardNumber) => cardNumberToType(cardNumber, numPlayers)}
              cardNumberToColor={this.cardNumberToColor.bind(this)}
              canDeclare={key === this.props.user && (gameState === GAME_STATE.PLAYING || this.state.roomData.currentPlayer === key)}
              onDeclare={this.onDeclare.bind(this)}
              cardOnMouseEnter={(cardNumber) => this.cardOnMouseEnter(this.props.user, cardNumber)}
              cardOnMouseLeave={() => this.cardOnMouseLeave(this.props.user)}
              activeCard={this.state.roomData.activeCard}
              cardOnClick={(cardNumber) => this.cardOnClick(this.props.user, cardNumber, key)}
              discoveredCards={this.state.discoveredCards}
              playerIcon={(key === this.state.roomData.currentPlayer ? 'lightbulb' : 'user circle')}
              playerColor={(key === this.state.roomData.currentPlayer ? 'yellow' : '')}
              gameState={this.state.roomData.status}
              roomCode={this.props.roomCode}
            />
          )
        )
    }

    let daysData
    if (this.state.roomData && this.state.roomData.day1) {
      const day1 = { day: 'Day 1', cards: Object.values(this.state.roomData.day1) }
      const day2 = { day: 'Day 2', cards: Object.values(this.state.roomData.day2) }
      const day3 = { day: 'Day 3', cards: Object.values(this.state.roomData.day3) }
      const day4 = { day: 'Day 4', cards: Object.values(this.state.roomData.day4) }
      daysData = [day1, day2, day3, day4]
    }

    const ret = (
      <Grid centered columns='2'>
        <Grid.Column>
          <Grid centered divided stackable columns='4'>{playerHands}</Grid>
          <DiscoveredDaysDisplay days={daysData} cardNumberToType={(number) => cardNumberToType(number, numPlayers)} cardNumberToColor={this.cardNumberToColor.bind(this)}/>
          <WaitingRoomPlayersDisplay roomData={this.state.roomData} userId={this.props.user}/>
          <RoomInfoDisplay roomCode={this.props.roomCode} roomData={this.state.roomData} userId={this.props.user}/>
          <Rail position='left'>
            <Container>
              <GameHistoryDisplay
                roomData={this.state.roomData}
                players={this.state.players}
                roomCode={this.props.roomCode}
                cardNumberToType={(cardNumber) => cardNumberToType(cardNumber, numPlayers)}
              />
            </Container>
          </Rail>
          <Rail position='right'>
            <Container>
              <RoomCardInfo numPlayers={numPlayers} discoveredCards={this.state.discoveredCards} claims={claims} gameState={gameState} />
            </Container>
          </Rail>
        </Grid.Column>
      </Grid>
    )

    trace.stop()
    return ret
  }
}

RoomBase.propTypes = {
  user: PropTypes.string,
  roomCode: PropTypes.string,
  isHistory: PropTypes.bool
}

export default function Room (props) {
  const { roomCode } = useParams()
  return (
    <RoomBase roomCode={roomCode} {...props} key={roomCode} />
  )
}
