import React from 'react'
import { render, screen } from '@testing-library/react'
import CardTypeCountDisplay from './CardTypeCountDisplay'

describe('Card Count Display', () => {
  // Applies only to tests in this describe block
  beforeEach(() => {
    render(<CardTypeCountDisplay
      cthulhus={1}
      signs={4}
    />)
  })

  test('renders', () => {
    expect(screen.getByText('Cthulhus')).toBeInTheDocument()
    expect(screen.getByText('Signs')).toBeInTheDocument()
    expect(screen.getByText('1')).toBeInTheDocument()
    expect(screen.getByText('4')).toBeInTheDocument()
  })
})
