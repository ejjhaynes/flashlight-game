const perf = window.perf

export function createTrace (traceId) {
  return perf.trace(traceId)
}
