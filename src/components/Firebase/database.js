const firebase = window.firebase

export function onValueChange (path, func) {
  firebase.database().ref(path).on('value', func)
}

export function onceValue (path, func) {
  return firebase.database().ref(path).once('value', func)
}

export function setValue (path, value, override = true) {
  if (override || !(value instanceof Map)) {
    return firebase.database().ref(path).set(value)
  } else {
    return Promise.all(
      Array.from(value.keys())
        .map((k) => firebase.database().ref(path + '/' + k.toString()).set(value.get(k)))
    )
  }
}

export function pushValue (path, value) {
  return firebase.database().ref(path).push(value)
}
